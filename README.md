#Domain names

* Install nodejs and git:

`sudo apt-get install git`

`sudo apt-get install nodejs`

* Configure git:

`git config --global user.email "name@email.com"`

`git config --global user.name "name"`



* Clone project:

`git clone https://dimhold@bitbucket.org/dimhold/domains.git`

`cd domains`

* Add remote url with autorization:

`git remote set-url origin https://domainmaniac:123456@bitbucket.org/dimhold/domains.git`

* Create folder with unic name:

`mkdir my-unic-folder`

* Run N domain.js winth start domain and last domain:

`nohup nodejs domain.js aaaaaaaa  azzzzzzz > aaaaaaaa-azzzzzzz.json &`

* Run monitoring.js:

`nohup nodejs monitoring.js > my-unic-folder/my-unic-folder.log &`

* Add monitoring output to git:

`git add my-unic-folder`

* Run push.js:

`nohup nodejs push.js &`

