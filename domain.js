var DNS = require("dns");

var shift = {'9': '-', '-': 'a'};
var first = '0';
var overflow = {'z': first};

var lastDomain = null;

//http://www.iana.org/domains/root/db
var zones = [
".ac",	".ad",	".ae",	".aero", ".af",	".ag",	".ai",	".al",	".am",	".an",	".ao",	".aq",	".ar",	".arpa", ".as",	".asia", ".at",	".au",	".aw",	".ax",	".az",	".ba",	".bb", ".bd",	".be",	".bf",	".bg",	".bh",	".bi",	".biz",	".bj",	".bl",	".bm",	".bn",	".bo", ".bq",	".br",	".bs",	".bt",	".bv",	".bw",	".by", ".bz",	".ca", ".cat",	".cc", ".cd",	".cf",	".cg",	".ch",	".ci",	".ck",	".cl",	".cm",	".cn",	".co",	".com",	".coop", ".cr",	".cu",	".cv",	".cw",	".cx",	".cy",	".cz",	".de",	".dj",	".dk",	".dm",	".do", ".dz",	".ec",	".edu",	".ee",	".eg",	".eh",	".er",	".es",	".et",	".eu",	".fi",	".fj",	".fk",	".fm",	".fo",	".fr",	".ga", ".gb",	".gd",	".ge",	".gf",	".gg",	".gh",	".gi",	".gl",	".gm",	".gn",	".gov", ".gp",	".gq",	".gr",	".gs",	".gt",	".gu",	".gw",	".gy",	".hk",	".hm",	".hn",	".hr",	".ht",	".hu",	".id",	".ie",	".il",	".im",	".in",	".info", ".int",	".io",	".iq",	".ir",	".is",	".it",	".je",	".jm",	".jo",	".jobs", ".jp",	".ke",	".kg",	".kh",	".ki",	".km",	".kn",	".kp",	".kr",	".kw",	".ky",	".kz",	".la", ".lb",	".lc",	".li",	".lk",	".lr",	".ls",	".lt",	".lu",	".lv", ".ly",	".ma",	".mc",	".md",	".me",	".mf",	".mg",	".mh",	".mil",	".mk",	".ml",	".mm",	".mn",	".mo",	".mobi", ".mp",	".mq",	".mr",	".ms",	".mt",	".mu",	".museum", ".mv",	".mw",	".mx",	".my",	".mz",	".na",	".name", ".nc",	".ne",	".net",	".nf",	".ng",	".ni",	".nl",	".no",	".np",	".nr",	".nu",	".nz",	".om",	".org",	".pa",	".pe",	".pf",	".pg", ".ph",	".pk",	".pl",	".pm",	".pn",	".post", ".pr",	".pro", ".ps", ".pt",	".pw",	".py",	".qa", ".re",	".ro",	".rs",	".ru",	".rw",	".sa",	".sb",	".sc",	".sd",	".se",	".sg",	".sh",	".si",	".sj",	".sk",	".sl",	".sm",	".sn",	".so",	".sr",	".ss",	".st",	".su",	".sv",	".sx",	".sy",	".sz",	".tc",	".td",	".tel",	".tf",	".tg",	".th",	".tj",	".tk",	".tl",	".tm",	".tn",	".to", ".tp",	".tr",	".travel", ".tt",	".tv",	".tw",	".tz",	".ua",	".ug",	".uk",	".um",	".us",	".uy",	".uz", ".va",	".vc",	".ve",	".vg",	".vi", ".vn", ".vu",	
".wf",	".ws",	".xxx",	".ye",	".yt",	".za",	".zm",	".zw"];


function next(domain) {
	var zone = /\..*$/.exec(domain)[0]
	domain = domain.replace(zone, "");
	var domain = inc(domain.split(""), domain.length - 1);
	return domain.join("") + zone;
}

function inc(domain, i) {
	if (i < 0) {
		domain = [first].concat(domain);
	} else if (overflow[domain[i]]) {
		domain[i] = overflow[domain[i]];
		return inc(domain, --i);
	} else if (shift[domain[i]]) {
		domain[i] = shift[domain[i]];
	} else {
		domain[i] = String.fromCharCode(domain[i].charCodeAt(0) + 1);
	}

	return domain;
}


function goToInfinity(domain) {
	for (var i = 0; i < zones.length; i++) {
		lookup(domain + zones[i]);
	}
}

function lookup(domain) {
	if (shouldStop(domain)) {
		return;
	}

	DNS.lookup(domain, function (e, ip) {
		if (ip) {
			console.log('{"' + domain + '":"' + ip + '"},');
		}

		lookup( next(domain) );
	});
}

function shouldStop(domain) {
	if (domain.replace(/\..*$/, "") == lastDomain) {
		return true;
	}
	return false;
}

function main() {
	var domain = process.argv[2];
	var lastDomainArg = process.argv[3];
	if (domain) {
		domain = domain.replace(/\..*$/, "");
	} else {
		console.log("You don't specify begin domain(e.g.: nodejs domain aaa.com). So ok, I starting with '00'.com");
		domain = "00";
	}

	if (lastDomainArg) {
		lastDomain = lastDomainArg.replace(/\..*$/, "");
	}

	goToInfinity(domain);
}

function test () {
	var data = [{input: "azz.by", expect: "b00.by"},
				{input: "az0.info", expect: "az1.info"},
				{input: "az9.com", expect: "az-.com"},
				{input: "az-.co", expect: "aza.co"},
				{input: "zzz.cc", expect: "0000.cc"},
				{input: ".com", expect: "0.com"},
				{input: "a-z.com", expect: "aa0.com"}]


	for (var i = 0; i < data.length; i++) {
		if (next(data[i].input) == data[i].expect) {
			console.log("ok " + i);
		} else {
			console.log("Not equals! For " + data[i].input + " expect: " + data[i].expect + " but return " + next[i]);
		}
	}

}

// test();

main();


